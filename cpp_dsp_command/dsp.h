#pragma once
#include "header.h"


class dsp {
public:
	dsp();
	~dsp() {};

	int version;
	unsigned int pixels = 0x10000;
	int multi_trigger = 1;

	unsigned int rg_dsp_set_start_addr = 0x5000000c;
	unsigned int rg_dsp_set_end_addr = 0x50000010;
	unsigned int rg_dsp_half_image_width = 0x50000158;
	unsigned int rg_dsp_trigger = 0x50000008;
	unsigned int rg_spi_base_addr = 0x2000000c;
	unsigned int sram_start_addr = 0x00010000;

	unsigned int instr_end_addr;
	unsigned int addr1_base = 0x0000;
	unsigned int addr2_base = addr1_base + pixels * 2;
	unsigned int output_base = addr1_base;//addr2_base + pixels * 2;
	unsigned int instr_start_addr = sram_start_addr + addr2_base + pixels * 2;//0x00000000 + output_base + pixels * 2;
	short constant1 = -100;
	short constant2 = -200;
	unsigned int dsp_done_addr = 0x5000015c;
	unsigned int dsp_done_clear_addr = 0x50000160;

	unsigned int half_image_width = 20;

	unsigned int shift_in_1  = 2;
	unsigned int shift_in_2  = 2;
	unsigned int shift_out_1 = 3;
	unsigned int shift_out_2 = 0;
	unsigned int set_8bit_output_en = 0;
	unsigned int set_matrix_in = 9;
	unsigned int set_matrix_out = 9;


	unsigned int cal_times = pixels / 2;

	fstream fp_instr;
	fstream fp_data;
	fstream fp_answer;
	fstream fp_data_riscv;
	fstream fp_instr_riscv;
	fstream fp_answer_riscv;
	fstream fp_table_riscv;

	string filename_data1;
	string filename_data2;
	string filename_answer;
	string dir_name;

	unsigned char *data_in_1;
	unsigned char *data_in_2;
	short *filter;
	short *table;
	short *data_16bit_1;
	short *data_16bit_2;
	short *data_8bit_2;
	short *data_out;

	void write_instr_set_1(int multi);
	void write_instr_set_2(int multi);
	void write_lut_instr();
	void write_lut_table();
	void write_data_in(unsigned char *data_in, string name, int randseed);
	void write_answer();
	void write_coeff_in(int randseed);

	unsigned int SET_ADDR1_BASE =	     0x01000000;
	unsigned int SET_ADDR2_BASE =		 0x02000000;
	unsigned int SET_OUTPUT_BASE =		 0x03000000;
	unsigned int SET_CONSTANT1 =		 0x04000000;
	unsigned int SET_CONSTANT2 =		 0x05000000;
	unsigned int SET_FLOAT_POINT_SHIFT = 0x06000000 + (shift_in_1 << 12) + (shift_in_2 << 8) + (shift_out_1 << 4) + shift_out_2;
	unsigned int SET_8BIT_OUTPUT =       0x07000000 + set_8bit_output_en;
	unsigned int SET_MATRIX_IN_OUT =     0x08000000 + (set_matrix_in << 4) + set_matrix_out;

	int matrix_output_idx;

	void ADD_16B_16B();
	void ADD_16B_8B();
	void ADD_16B_C1();
	void ADD_16B_C2();
	void SUB_16B_16B();
	void SUB_16B_8B();
	void SUB_16B_C1();
	void SUB_16B_C2();
	void MUL_16B_16B();
	void MUL_16B_8B();
	void MUL_16B_C1();
	void MUL_16B_C2();
	void MAC();
	void MATRIX_N_M();
	void LOOK_UP();
	void CONVOLUTION();


	void COMPUTATION();
	void parse_for_riscv();


	int output_number;
	int answer_addr;
	
};
