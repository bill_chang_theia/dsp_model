#include "header.h"
#include "dsp.h"


dsp::dsp() {


	instr_end_addr = 0;
	version = 100;

	dir_name = "dsp_instr_8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version);
	
#ifdef _WIN32
    _mkdir(dir_name.c_str());
#elif __APPLE__
    mkdir(dir_name.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#else
#   error "Unknown compiler"
#endif
	
    filename_data1 = dir_name + "/" + "data_in_1_" + "8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version) + ".txt";
	filename_data2 = dir_name + "/" + "data_in_2_" + "8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version) + ".txt";
	filename_answer = dir_name + "/" + "anwser_" + "8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version) + ".txt";
	fp_instr.open(dir_name + "/" + "instrSet_" + to_string(version) + ".v", ios::out);
	fp_instr_riscv.open(dir_name + "/" + "instrSet_" + to_string(version) + "_riscv.v", ios::out);
	

	if (!fp_instr) {
		cout << "Fail to open file: " << "instrSet_" + to_string(version) + ".v" << endl;
	}

	for (int multi = 0; multi < multi_trigger; multi++)
	{
		if(multi == 0){
			data_in_1 = new unsigned char[pixels * 2]();
			data_in_2 = new unsigned char[pixels * 2]();
			data_16bit_1 = new short[pixels]();
			data_16bit_2 = new short[pixels]();
			data_8bit_2 = new short[pixels]();
			data_out = new short[pixels]();

			srand(time(NULL) + multi);
			int randseed = rand();
			write_data_in(data_in_1, filename_data1, randseed);
			randseed = rand();
			write_data_in(data_in_2, filename_data2, randseed);
			short tmp1, tmp2;


			fp_data_riscv.open(dir_name + "/" + "data_in_1_" + "8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version) + "_riscv.txt", ios::out);
			for (int i = 0; i < pixels * 2; i = i + 2)
			{
				tmp1 = data_in_1[i];
				tmp2 = data_in_1[i + 1] << 8;
				data_16bit_1[i / 2] = tmp2 | tmp1;
			}
			for (int i = 0; i < pixels; i = i + 2)
			{
				unsigned int data_value_1 = data_16bit_1[i];
				unsigned int data_value_2 = data_16bit_1[i + 1] << 16;
				unsigned int data_value = (data_value_1 & 0x0000ffff) | (data_value_2 & 0xffff0000);

				fp_data_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << sram_start_addr + addr1_base + i * 2 << ", 0x" << hex << setw(8) << setfill('0') << data_value << ");" << endl;
			}
			fp_data_riscv.close();

			fp_data_riscv.open(dir_name + "/" + "data_in_2_" + "8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version) + "_riscv.txt", ios::out);
			for (int i = 0; i < pixels * 2; i = i + 2)
			{
				tmp1 = data_in_2[i];
				tmp2 = data_in_2[i + 1] << 8;
				data_16bit_2[i / 2] = tmp2 | tmp1;
			}
			for (int i = 0; i < pixels; i = i + 2)
			{
				unsigned int data_value_1 = data_16bit_2[i];
				unsigned int data_value_2 = data_16bit_2[i + 1] << 16;
				unsigned int data_value = (data_value_1 & 0x0000ffff) | (data_value_2 & 0xffff0000);
				fp_data_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << sram_start_addr + addr2_base + i * 2 << ", 0x" << hex << setw(8) << setfill('0') << data_value << ");" << endl;
			}
			fp_data_riscv.close();
			for (int i = 0; i < pixels; i = i + 1)
			{
				data_8bit_2[i] = data_in_2[i] << 8;
			}
		}
		else {
			for (int i = 0; i < pixels; i = i + 2)
			{
				data_8bit_2[i] = (data_16bit_1[i/2] & 0x00ff) << 8;
				data_8bit_2[i + 1] = data_16bit_1[i/2] & 0xff00;
			}
		}
		if(multi == 0)
			write_instr_set_1(multi);
		else {
			instr_end_addr = instr_start_addr - 4;
		}

		COMPUTATION();


		filename_answer = dir_name + "/" + "anwser_" + "8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version) + "_" + to_string(multi) + ".txt";

		write_instr_set_2(multi);

		write_answer();
		
	}
	fp_data.close();
	fp_instr.close();
	fp_instr_riscv.close();


	parse_for_riscv();


}

void dsp::write_instr_set_1(int multi) {
	 
	write_lut_table();
	write_lut_instr();

	//	write data_in_1
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_spi_base_addr << ", 32'h" << hex << setw(8) << setfill('0') << sram_start_addr + addr1_base << ");" << endl;
	fp_instr << "SPI_Slave_Burst_Write_Data(\"spi_instr/" << filename_data1 << "\");" << endl;
	//	write data_in_2
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_spi_base_addr << ", 32'h" << hex << setw(8) << setfill('0') << sram_start_addr + addr2_base << ");" << endl;
	fp_instr << "SPI_Slave_Burst_Write_Data(\"spi_instr/" << filename_data2 << "\");" << endl;

	write_coeff_in(rand());

	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_dsp_set_start_addr << ", 32'h" << hex << setw(8) << setfill('0') << instr_start_addr << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << rg_dsp_set_start_addr << ", 0x" << hex << setw(8) << setfill('0') << instr_start_addr << ");" << endl;

	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_dsp_half_image_width << ", 32'h" << hex << setw(8) << setfill('0') << half_image_width << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << rg_dsp_half_image_width << ", 0x" << hex << setw(8) << setfill('0') << half_image_width << ");" << endl;

	instr_end_addr = instr_start_addr;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SET_ADDR1_BASE + sram_start_addr + addr1_base << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SET_ADDR1_BASE + sram_start_addr + addr1_base << ");" << endl;

	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SET_ADDR2_BASE + sram_start_addr + addr2_base << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SET_ADDR2_BASE + sram_start_addr + addr2_base << ");" << endl;

	instr_end_addr = instr_end_addr + 4;

	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SET_OUTPUT_BASE + sram_start_addr + output_base << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SET_OUTPUT_BASE + sram_start_addr + output_base << ");" << endl;

	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SET_CONSTANT1 + (constant1 & 0x00ffffff) << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SET_CONSTANT1 + (constant1 & 0x00ffffff) << ");" << endl;

	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SET_CONSTANT2 + (constant2 & 0x00ffffff) << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SET_CONSTANT2 + (constant2 & 0x00ffffff) << ");" << endl;

	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SET_FLOAT_POINT_SHIFT  << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SET_FLOAT_POINT_SHIFT << ");" << endl;

	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SET_8BIT_OUTPUT << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SET_8BIT_OUTPUT << ");" << endl;

	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SET_MATRIX_IN_OUT << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SET_MATRIX_IN_OUT << ");" << endl;
	

}

void dsp::write_instr_set_2(int multi) {

	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_dsp_set_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << rg_dsp_set_end_addr << ", 0x" << hex << setw(8) << setfill('0') << instr_end_addr << ");" << endl;

	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_dsp_trigger << ", 32'h" << hex << setw(8) << setfill('0') << 1 << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << rg_dsp_trigger << ", 0x" << hex << setw(8) << setfill('0') << 1 << ");" << endl;


	fp_instr << "wait(et760_top_dut.et760_top_inst.dsp_done) begin\n";
	fp_instr << "//igis_replace_wait SPI_Slave_Single_Read_32bit(8'h32, 32'h" << hex << setw(8) << setfill('0') << dsp_done_addr << ", 32'h" << hex << setw(8) << setfill('0') << 0x00000001 << ")" << endl;

	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_spi_base_addr << ", 32'h" << hex << setw(8) << setfill('0') << sram_start_addr + output_base << ");" << endl;

	fp_instr << "SPI_Slave_Burst_Read_Data(\"spi_instr/" << filename_answer << "\");" << endl;
	fp_instr << "end" << endl;

	fp_instr << "wait(et760_top_dut.et760_top_inst.dsp_done) begin\n";
	fp_instr << "//igis_replace_wait SPI_Slave_Single_Read_32bit(8'h32, 32'h" << hex << setw(8) << setfill('0') << dsp_done_addr << ", 32'h" << hex << setw(8) << setfill('0') << 0x00000001 << ")" << endl;

	fp_instr << "@(posedge clk);\n";
	fp_instr << "@(posedge clk);\n";
	fp_instr << "@(posedge clk);\n";
	fp_instr << "$display(\"-----------dsp_done_clear-----------\");\n";
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << dsp_done_clear_addr << ", 32'h00000001);" << endl;
	fp_instr << "end" << endl;

}


void dsp::write_lut_instr() {

	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_spi_base_addr << ", 32'h" << hex << setw(8) << setfill('0') << sram_start_addr + addr1_base << ");" << endl;
	
	fp_instr << "SPI_Slave_Burst_Write_Data(\"spi_instr/" << dir_name + "/" + "table_" + to_string(version) + ".v" << "\");" << endl;

	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_dsp_set_start_addr << ", 32'h" << hex << setw(8) << setfill('0') << instr_start_addr << ");" << endl;
	fp_table_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << rg_dsp_set_start_addr << ", 0x" << hex << setw(8) << setfill('0') << instr_start_addr << ");" << endl;

	instr_end_addr = instr_start_addr;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SET_ADDR1_BASE + addr1_base << ");" << endl;
	fp_table_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SET_ADDR1_BASE + addr1_base << ");" << endl;

	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << 0x92000000 + 128 << ");" << endl;
	fp_table_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << 0x92000000 + 128 << ");" << endl;

	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_dsp_set_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ");" << endl;
	fp_table_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << rg_dsp_set_end_addr << ", 0x" << hex << setw(8) << setfill('0') << instr_end_addr << ");" << endl;

	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_dsp_trigger << ", 32'h" << hex << setw(8) << setfill('0') << 1 << ");" << endl;
	fp_table_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << rg_dsp_trigger << ", 0x" << hex << setw(8) << setfill('0') << 1 << ");" << endl;

	fp_instr << "wait(et760_top_dut.et760_top_inst.dsp_done) begin\n";
	fp_instr << "//igis_replace_wait SPI_Slave_Single_Read_32bit(8'h32, 32'h" << hex << setw(8) << setfill('0') << dsp_done_addr << ", 32'h" << hex << setw(8) << setfill('0') << 0x00000001 << ")" << endl;

	fp_instr << "@(posedge clk);\n";
	fp_instr << "$display(\"-----------dsp_done_clear-----------\");\n";
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << dsp_done_clear_addr << ", 32'h00000001);" << endl;
	fp_instr << "end";
	fp_instr << "//******************************************************************************************************************************//" << endl;
	fp_instr << "//*********************************************     LOAD LUT DONE    ***********************************************************//" << endl;
	fp_instr << "//******************************************************************************************************************************//" << endl;
	fp_table_riscv.close();
}

void dsp::write_data_in(unsigned char *data_in, string name, int randseed) {

	fp_data.open( name, ios::out);
	if (!fp_data) {
		cout <<" Fail to open file: " << name << endl;
	}

	fp_data << "0x26" << endl << "0x00" << endl;

	for (int i = 0; i < pixels * 2; i++)
	{
		unsigned int value = abs(randseed + rand() + i ) % 256;

		data_in[i] = value;
		fp_data << "0x" << hex << setw(2) << setfill('0') << value << endl;
	}
	

	fp_data.close();
}

void dsp::write_lut_table() {
	//	build table
	fstream fp_table;
	
	fp_table.open(dir_name + "/" + "table_" + to_string(version) + ".v", ios::out);
	fp_table_riscv.open(dir_name + "/" + "table_" + to_string(version) + "_riscv.v", ios::out);

	fp_table << "0x26" << endl << "0x00" << endl;
	short tmp;
	table = new short[256]();
	for (int i = 0; i < 256; i++)
	{
		table[i] = (rand() + i) % 256;
		fp_table << "0x" << hex << setw(2) << setfill('0') << table[i] << endl;
		tmp = (rand() + i) % 256;
		table[i] = table[i] + (tmp << 8);
		fp_table << "0x" << hex << setw(2) << setfill('0') << tmp << endl;
	}

	for (int i = 0; i < 256; i++)
	{
		unsigned int data_value_1 = table[i];
		unsigned int data_value_2 = table[i + 1] << 16;
		unsigned int data_value = (data_value_1 & 0x0000ffff) | (data_value_2 & 0xffff0000);

		fp_table_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << sram_start_addr + addr1_base + i * 4 << ", 0x" << hex << setw(8) << setfill('0') << data_value << ");" << endl;
	}


	
	fp_table.close();
}

void dsp::write_coeff_in(int randseed) {

	unsigned int rg_dsp_coeff = 0x50000014;
	int matrix_in = (SET_MATRIX_IN_OUT & 0x000000f0) >> 4;
	filter = new short[81]();

	for (int i = 0; i < 81; i++)
	{
		filter[i] = 0;
	}

	for (int i = 0; i < 9; i++)
	{
		for (int j = 0;j < 9;j++) {
			if(j < matrix_in){
				unsigned short value = (randseed + rand() + i) % 4096;
				value = value << 4;
				value = (short)value >> 4;
				filter[i * 9 + j] = value;
				fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_dsp_coeff << ", 32'h" << hex << setw(8) << setfill('0') << value << ");" << endl;
				fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << rg_dsp_coeff << ", 0x" << hex << setw(8) << setfill('0') << value << ");" << endl;
		
				rg_dsp_coeff += 4;
			}
			else {
				fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << rg_dsp_coeff << ", 32'h" << hex << setw(8) << setfill('0') << 0 << ");" << endl;
				fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << rg_dsp_coeff << ", 0x" << hex << setw(8) << setfill('0') << 0 << ");" << endl;

				rg_dsp_coeff += 4;
			}
		}
	}

}


void dsp::write_answer()
{
	fp_answer_riscv.open(dir_name + "/" + "anwser_" + "8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version) + "_riscv.txt", ios::out);
	fp_answer.open(filename_answer, ios::out);
	if (!fp_answer) {
		cout << " Fail to open file: " << filename_answer << endl;
	}

	answer_addr = instr_end_addr + 4;

	fp_answer << "0x22" << endl << "0x00" << endl;

	unsigned char data_out_tmp;

	for (int i = 0; i < pixels; i++)
	{
		data_out_tmp = data_16bit_1[i];
		fp_answer << "0x" << hex << setw(2) << setfill('0') << (unsigned short)data_out_tmp << endl;

		data_out_tmp = (data_16bit_1[i] >> 8);
		fp_answer << "0x" << hex << setw(2) << setfill('0') << (unsigned short)data_out_tmp << endl;
	}

	for (int i = 0; i < pixels; i = i + 2)
	{
		unsigned int data_value_1 = data_16bit_1[i];
		unsigned int data_value_2 = data_16bit_1[i + 1] << 16;
		unsigned int data_value = (data_value_1 & 0x0000ffff) | (data_value_2 & 0xffff0000);

		fp_answer_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << answer_addr + i * 2 << ", 0x" << hex << setw(8) << setfill('0') << data_value << ");" << endl;
	}


	fp_answer_riscv.close();
	fp_answer.close();

}

void dsp::ADD_16B_16B() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << ADD_16_16 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << ADD_16_16 + cal_times << ");" << endl;

	int data_tmp;
	for (int i = 0; i < pixels; i++)
	{
		data_tmp = (int)data_16bit_1[i] + (int)data_16bit_2[i];
		if (data_tmp > 32767)	data_tmp = 32767;
		else if (data_tmp < -32768) data_tmp = -32768;

		data_out[i] = data_tmp;
		output_number = i;
	}

	if(set_8bit_output_en == 0){
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i/2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}

}

void dsp::ADD_16B_8B() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << ADD_16_8 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << ADD_16_8 + cal_times << ");" << endl;
	int data_tmp;
	for (int i = 0; i < pixels; i++)
	{
		data_tmp = (int)data_16bit_1[i] + (int)data_8bit_2[i];
		if (data_tmp > 32767)	data_tmp = 32767;
		else if (data_tmp < -32768) data_tmp = -32768;
		data_out[i] = data_tmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}

void dsp::ADD_16B_C1() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << ADD_16_C1 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << ADD_16_C1 + cal_times << ");" << endl;

	int data_tmp;
	for (int i = 0; i < pixels; i++)
	{
		data_tmp = (int)data_16bit_1[i] + (int)constant1;
		if (data_tmp > 32767)	data_tmp = 32767;
		else if (data_tmp < -32768) data_tmp = -32768;
		data_out[i] = data_tmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}


void dsp::ADD_16B_C2() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << ADD_16_C2 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << ADD_16_C2 + cal_times << ");" << endl;
	int data_tmp;
	for (int i = 0; i < pixels; i++)
	{
		data_tmp = (int)data_16bit_1[i] + (int)constant2;
		if (data_tmp > 32767)	data_tmp = 32767;
		else if (data_tmp < -32768) data_tmp = -32768;
		data_out[i] = data_tmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}


void dsp::SUB_16B_16B() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SUB_16_16 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SUB_16_16 + cal_times << ");" << endl;

	int data_tmp;
	for (int i = 0; i < pixels; i++)
	{
		data_tmp = (int)data_16bit_1[i] - (int)data_16bit_2[i];
		if (data_tmp > 32767)	data_tmp = 32767;
		else if (data_tmp < -32768) data_tmp = -32768;
		data_out[i] = data_tmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}

void dsp::SUB_16B_8B() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SUB_16_8 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SUB_16_8 + cal_times << ");" << endl;
	int data_tmp;
	for (int i = 0; i < pixels; i++)
	{
		data_tmp = (int)data_16bit_1[i] - (int)data_8bit_2[i];
		if (data_tmp > 32767)	data_tmp = 32767;
		else if (data_tmp < -32768) data_tmp = -32768;
		data_out[i] = data_tmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}

void dsp::SUB_16B_C1() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SUB_16_C1 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SUB_16_C1 + cal_times << ");" << endl;

	int data_tmp;
	for (int i = 0; i < pixels; i++)
	{
		data_tmp = (int)data_16bit_1[i] - (int)constant1;
		if (data_tmp > 32767)	data_tmp = 32767;
		else if (data_tmp < -32768) data_tmp = -32768;
		data_out[i] = data_tmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}

void dsp::SUB_16B_C2() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << SUB_16_C2 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << SUB_16_C2 + cal_times << ");" << endl;

	int data_tmp;
	for (int i = 0; i < pixels; i++)
	{
		data_tmp = (int)data_16bit_1[i] - (int)constant2;
		if (data_tmp > 32767)	data_tmp = 32767;
		else if (data_tmp < -32768) data_tmp = -32768;
		data_out[i] = data_tmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}

void dsp::MUL_16B_16B() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << MUL_16_16 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << MUL_16_16 + cal_times << ");" << endl;
	int mul_tmp;
	int tmptmp;
	for (int i = 0; i < pixels; i++)
	{
		mul_tmp = (short)(data_16bit_1[i] >> ((SET_FLOAT_POINT_SHIFT & 0x0000f000) >> 12)) * (short)(data_16bit_2[i] >> ((SET_FLOAT_POINT_SHIFT & 0x00000f00) >> 8));
		tmptmp = (mul_tmp >> ((SET_FLOAT_POINT_SHIFT & 0x000000f0) >> 4));
		if (tmptmp >= 32767)
			tmptmp = 32767;
		else if (tmptmp <= -32768)
			tmptmp = -32768;
		data_out[i] = tmptmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}

}

void dsp::MUL_16B_8B() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << MUL_16_8 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << MUL_16_8 + cal_times << ");" << endl;

	int mul_tmp;
	int tmptmp;
	for (int i = 0; i < pixels; i++)
	{
		mul_tmp = (short)(data_16bit_1[i] >> ((SET_FLOAT_POINT_SHIFT & 0x0000f000) >> 12)) * (short)(data_8bit_2[i] >> ((SET_FLOAT_POINT_SHIFT & 0x00000f00) >> 8));
		tmptmp = (mul_tmp >> ((SET_FLOAT_POINT_SHIFT & 0x000000f0) >> 4));
		if (tmptmp >= 32767)
			tmptmp = 32767;
		else if (tmptmp <= -32768)
			tmptmp = -32768;
		data_out[i] = tmptmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}

}

void dsp::MUL_16B_C1() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << MUL_16_C1 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << MUL_16_C1 + cal_times << ");" << endl;

	int mul_tmp;
	int tmptmp;
	for (int i = 0; i < pixels; i++)
	{
		mul_tmp = (short)(data_16bit_1[i] >> ((SET_FLOAT_POINT_SHIFT & 0x0000f000) >> 12)) * (short)(constant1 >> ((SET_FLOAT_POINT_SHIFT & 0x00000f00) >> 8));
		tmptmp = (mul_tmp >> ((SET_FLOAT_POINT_SHIFT & 0x000000f0) >> 4));
		if (tmptmp >= 32767)
			tmptmp = 32767;
		else if (tmptmp <= -32768)
			tmptmp = -32768;
		data_out[i] = tmptmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}

}


void dsp::MUL_16B_C2() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << MUL_16_C2 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << MUL_16_C2 + cal_times << ");" << endl;

	int mul_tmp;
	int tmptmp;
	for (int i = 0; i < pixels; i++)
	{
		mul_tmp = (short)(data_16bit_1[i] >> ((SET_FLOAT_POINT_SHIFT & 0x0000f000) >> 12)) * (short)(constant2 >> ((SET_FLOAT_POINT_SHIFT & 0x00000f00) >> 8));
		tmptmp = (mul_tmp >> ((SET_FLOAT_POINT_SHIFT & 0x000000f0) >> 4));
		if (tmptmp >= 32767)
			tmptmp = 32767;
		else if (tmptmp <= -32768)
			tmptmp = -32768;
		data_out[i] = tmptmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}

void dsp::MAC() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << MAC_16_C1_C2 + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << MAC_16_C1_C2 + cal_times << ");" << endl;

	int data_tmp;
	int mul_tmp;
	int tmptmp;
	for (int i = 0; i < pixels; i++)
	{
		data_tmp = (int)data_16bit_1[i] - (int)constant1;
		if (data_tmp > 32767)	data_tmp = 32767;
		else if (data_tmp < -32768) data_tmp = -32768;

		mul_tmp = (short)(data_tmp >> ((SET_FLOAT_POINT_SHIFT & 0x0000f000) >> 12)) * (short)(constant2 >> ((SET_FLOAT_POINT_SHIFT & 0x00000f00) >> 8));
		tmptmp = (mul_tmp >> ((SET_FLOAT_POINT_SHIFT & 0x000000f0) >> 4));
		if (tmptmp >= 32767)
			tmptmp = 32767;
		else if (tmptmp <= -32768)
			tmptmp = -32768;
		data_out[i] = tmptmp;
		output_number = i;
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}

void dsp::MATRIX_N_M() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << MATRIX + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << MATRIX + cal_times << ");" << endl;

	short matrix_9x1[9] = { 0 };
	short matrix_9x1_output[9] = { 0 };
	int matrix_in = (SET_MATRIX_IN_OUT & 0x000000f0) >> 4;
	int matrix_out = SET_MATRIX_IN_OUT & 0x0000000f;
	matrix_output_idx = 0;
	output_number = 0;
	for (int p = 0; p < pixels; p = p + matrix_in)
	{
		for (int i = 0; i < matrix_in; i++)
		{
			matrix_9x1[i] = data_16bit_1[p + i];
			matrix_9x1_output[i] = 0;
		}
		for (int n = 0; n < 9; n++)
		{
			int sum = 0;

			for (int m = 0; m < matrix_in; m++)
			{
				int shift = (filter[n * 9 + m] * matrix_9x1[m]) >> ((SET_FLOAT_POINT_SHIFT & 0x000000f0) >> 4);
				if (shift > 32767)
					shift = 32767;
				else if (shift < -32768)
					shift = -32768;

				sum += shift;
			}

			sum = sum >> ((SET_FLOAT_POINT_SHIFT & 0x0000000f));
			if (sum > 32767)
				sum = 32767;
			else if (sum < -32768)
				sum = -32768;

			matrix_9x1_output[n] = (short)sum;


		}
		for (int i = 0; i < matrix_out; i++)
		{
			data_out[matrix_output_idx] = matrix_9x1_output[8 - i];
			output_number = matrix_output_idx;
			matrix_output_idx++;
		}
	}
	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}

}

void dsp::LOOK_UP() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << LUT + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << LUT + cal_times << ");" << endl;

	int data_0;
	int data_1;
	unsigned int index;
	unsigned int offset;
	int lut_result;
	int last_8bit;

	for (int i = 0; i < pixels; i++)
	{
		index = ((unsigned short)data_16bit_1[i]) >> 8;
		offset = data_16bit_1[i] & 0x00ff;

		data_0 = table[index];
		if (index + 1 > 255)
			data_1 = table[index];
		else
			data_1 = table[index + 1];

		lut_result = data_0 * (256 - offset) + data_1 * offset;
		last_8bit = lut_result & 0x000000ff;
		if (last_8bit >= 128) {
			lut_result = lut_result >> 8;
			lut_result = lut_result + 1;
		}
		else {
			lut_result = lut_result >> 8;
		}

		lut_result = lut_result >> ((SET_FLOAT_POINT_SHIFT & 0x0000000f));

		if (lut_result > 32767)
			lut_result = 32767;
		else if (lut_result < -32768)
			lut_result = -32768;

		data_out[i] = lut_result;
		output_number = i;
	}

	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}

void dsp::CONVOLUTION() {
	instr_end_addr = instr_end_addr + 4;
	fp_instr << "SPI_Slave_Single_Write_32bit(8'h33, 32'h" << hex << setw(8) << setfill('0') << instr_end_addr << ", 32'h" << hex << setw(8) << setfill('0') << CONV + cal_times << ");" << endl;
	fp_instr_riscv << "set_dbg_val(" << "0x" << hex << setw(8) << setfill('0') << instr_end_addr << ", 0x" << hex << setw(8) << setfill('0') << CONV + cal_times << ");" << endl;

	int width = half_image_width * 2;
	int height = pixels / width;
	int j, k, f;
	int count = 0;

	int sum_tmp = 0;
	for (k = 4; k < height - 4; k++) {
		for (f = 4; f < width - 4; f++) {
			j = k * width + f;
			int sum = 0;
			for (int n = -4; n <= 4; n++)
			{
				for (int m = -4; m <= 4; m++)
				{
					int mul = (data_16bit_1[j + n * width + m] * filter[(n + 4) * 9 + m + 4]);
					int shift = mul >> ((SET_FLOAT_POINT_SHIFT & 0x000000f0) >> 4);

					if (shift > 32767)
						shift = 32767;
					else if (shift < -32768)
						shift = -32768;

					sum = sum + shift;
					sum_tmp = sum_tmp + shift;
				}

				sum_tmp = 0;
			}

			sum = sum >> ((SET_FLOAT_POINT_SHIFT & 0x0000000f));

			if (sum > 32767)
				sum = 32767;
			else if (sum < -32768)
				sum = -32768;

			data_out[count] = sum;
			output_number = count;
			count++;
		}
	}

	if (set_8bit_output_en == 0) {
		for (int i = 0; i <= output_number; i++)
		{
			data_16bit_1[i] = data_out[i];
		}
	}
	else {
		for (int i = 0; i <= output_number; i = i + 2)
		{
			data_16bit_1[i / 2] = ((data_out[i] & 0xff00) >> 8) | (data_out[i + 1] & 0xff00);
		}
	}
}

void dsp::COMPUTATION() {

	//ADD_16B_16B();
	//SUB_16B_16B();
	//MUL_16B_8B();
    fstream fp_random;
    fp_random.open(dir_name + "/" + "instrSet_random.v", ios::out);
    
    for (int i = 0; i < 1; i++) {

        int state = rand() % 16;
        printf("State : %d\n",state);
        if(state == 0)
        {
            ADD_16B_16B();
            fp_random << "ADD_16B_16B" << endl;
        }
        else if(state == 1)
        {
            ADD_16B_8B();
            fp_random << "ADD_16B_8B" << endl;
        }
        else if(state == 2)
        {
            ADD_16B_C1();
            fp_random << "ADD_16B_C1" << endl;
        }
        else if(state == 3)
        {
            ADD_16B_C2();
            fp_random << "ADD_16B_C2" << endl;
        }
        else if(state == 4)
        {
            SUB_16B_16B();
            fp_random << "SUB_16B_16B" << endl;
        }
        else if(state == 5)
        {
            SUB_16B_8B();
            fp_random << "SUB_16B_8B" << endl;
        }
        else if(state == 6)
        {
            SUB_16B_C1();
            fp_random << "SUB_16B_C1" << endl;
        }
        else if(state == 7)
        {
            SUB_16B_C2();
            fp_random << "SUB_16B_C2" << endl;
        }
        else if(state == 8)
        {
            MUL_16B_16B();
            fp_random << "MUL_16B_16B" << endl;
        }
        else if(state == 9)
        {
            MUL_16B_8B();
            fp_random << "MUL_16B_8B" << endl;
        }
        else if(state == 10)
        {
            MUL_16B_C1();
            fp_random << "MUL_16B_C1" << endl;
        }
        else if(state == 11)
        {
            MUL_16B_C2();
            fp_random << "MUL_16B_C2" << endl;
        }
        else if(state == 12)
        {
            MAC();
            fp_random << "MAC" << endl;
        }
        else if(state == 13)
        {
            MATRIX_N_M();
            fp_random << "MATRIX_N_M" << endl;
        }
        else if(state == 14)
        {
            CONVOLUTION();
            fp_random << "CONVOLUTION" << endl;
        }
        else if(state == 15)
        {
            CONVOLUTION();
            fp_random << "CONVOLUTION" << endl;
        }
        else
            printf("[Error]<COMPUTATION> %d", state);
    }
    fp_random.close();
}


void dsp::parse_for_riscv() {

	fstream fp_riscv;
	fstream fp_data_1_riscv;
	fstream fp_data_2_riscv;
	char ch;
	fp_riscv.open(dir_name + "/" + "dsp_" + to_string(version) + "_riscv.c", ios::out);


	fp_data_1_riscv.open(dir_name + "/" + "data_in_1_" + "8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version) + "_riscv.txt", ios::in);
	fp_data_2_riscv.open(dir_name + "/" + "data_in_2_" + "8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version) + "_riscv.txt", ios::in);
	fp_instr_riscv.open(dir_name + "/" + "instrSet_" + to_string(version) + "_riscv.v", ios::in);
	fp_answer_riscv.open(dir_name + "/" + "anwser_" + "8bit_" + to_string(SET_8BIT_OUTPUT & 0x00000001) + "_" + to_string(version) + "_riscv.txt", ios::in);
	fp_table_riscv.open(dir_name + "/" + "table_" + to_string(version) + "_riscv.v", ios::in);

	fp_riscv << "#include <dspTest.h>" << endl;
	fp_riscv << "#if( " << "DSP_" + to_string(version) + "_RISCV"<< " == 1)" << endl;

	fp_riscv << "#include <dsp_" << to_string(version) << "_riscv.h>" << endl;
	fp_riscv << "extern void dspTest_" << to_string(version) << "();" << endl;
	fp_riscv << "void lookUpTable_" << to_string(version) << "();" << endl;
	fp_riscv << "void data_1_In_" << to_string(version) << "();" << endl;
	fp_riscv << "void data_2_In_" << to_string(version) << "();" << endl;
	fp_riscv << "void inStrSet_" << to_string(version) << "();" << endl;
	fp_riscv << "void dspAns_" << to_string(version) << "();" << endl;

	fp_riscv << "#define PIXEL_NUMBER_" + to_string(version) + " " << pixels << endl;
	fp_riscv << "#define DSP_OUTPUT_ADDR_" + to_string(version) + " " << "0x" << hex << setw(8) << setfill('0') << sram_start_addr + output_base << endl;
	fp_riscv << "#define DSP_ANS_ADDR_" + to_string(version) + " " << "0x" << hex << setw(8) << setfill('0') << answer_addr << endl;

	fp_riscv << endl << endl << endl;

	fp_riscv << "void dspTest_" << to_string(version) << "(){" << endl;

	fp_riscv << "lookUpTable_" << to_string(version) << "();" << endl;
	fp_riscv << "data_1_In_" << to_string(version) << "();" << endl;
	fp_riscv << "data_2_In_" << to_string(version) << "();" << endl;
	fp_riscv << "inStrSet_" << to_string(version) << "();" << endl;
	fp_riscv << "dspAns_" << to_string(version) << "();" << endl;
	fp_riscv << "compareAns((U16)PIXEL_NUMBER_" << to_string(version) << ");" << endl;



	fp_riscv << "}" << endl;

	fp_riscv << "void lookUpTable_" + to_string(version) + "(){" << endl;
		while (fp_table_riscv.peek() != EOF) {
			fp_table_riscv.get(ch);
			fp_riscv.put(ch);
		}
	fp_riscv << "}" << endl;


	fp_riscv << "void data_1_In_" + to_string(version) + "(){" << endl;
		while (fp_data_1_riscv.peek() != EOF) {
			fp_data_1_riscv.get(ch);
			fp_riscv.put(ch);
		}
	fp_riscv << "}" << endl;

	fp_riscv << "void data_2_In_" + to_string(version) + "(){" << endl;
		while (fp_data_2_riscv.peek() != EOF) {
			fp_data_2_riscv.get(ch);
			fp_riscv.put(ch);
		}
	fp_riscv << "}" << endl;


	fp_riscv << "void inStrSet_" + to_string(version) + "(){" << endl;
		while (fp_instr_riscv.peek() != EOF) {
			fp_instr_riscv.get(ch);
			fp_riscv.put(ch);
		}
	fp_riscv << "}" << endl;


	fp_riscv << "void dspAns_" + to_string(version) + "(){" << endl;
		while (fp_answer_riscv.peek() != EOF) {
			fp_answer_riscv.get(ch);
			fp_riscv.put(ch);
		}
	fp_riscv << "}" << endl;

	fp_riscv << "#endif " << endl;

}




