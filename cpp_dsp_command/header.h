#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <time.h>

#ifdef _WIN32
#include <direct.h>
#elif __APPLE__
#include <sys/stat.h>
#else
#   error "Unknown compiler"
#endif


using namespace std;

#define ADD_16_16	  0x10000000
#define ADD_16_8	  0x11000000
#define ADD_16_C1	  0x12000000
#define ADD_16_C2	  0x13000000
#define SUB_16_16	  0x30000000
#define SUB_16_8	  0x31000000
#define SUB_16_C1	  0x32000000
#define SUB_16_C2	  0x33000000
#define MUL_16_16	  0x50000000
#define MUL_16_8	  0x51000000
#define MUL_16_C1	  0x52000000
#define MUL_16_C2	  0x53000000
#define MAC_16_C1_C2  0x72000000
#define LOAD_COEFF    0xb2000000
#define CONV		  0xb3000000
#define MATRIX		  0xd2000000
#define LUT			  0x93000000